const fs =require('fs');
const Headers = require('./authentication')

//WP

const Request = require('./WPrequest')

async function PushData(DATA,Credentials , next){

    let WPReviews = 'https://' + Credentials.domain + '/wp-json/wp/v2/reviews';

    let WPauth = "Basic " + new Buffer(Credentials.username + ":" + Credentials.password).toString("base64");

    Headers.WPHeaders.headers.Authorization = WPauth;

    for (const item of DATA) {

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish"
        }

        JsonBody.title = item.title;
        JsonBody.content = item.content;

        JsonBody.fields = {
            link : item.link,
            name : item.name,
            lang : item.language,
            date : item.date,
            staydate: item.staydate,
            score: 5,
            review_from: item.review_from
        }

        const WPCreatePost = {
            url : WPReviews,
            headers : Headers.WPHeaders.headers,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost)

        console.log("eee")

    }

    //END
    next()
}

exports.push = PushData