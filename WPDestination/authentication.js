require('dotenv').config()

let WPusername = process.env.WP_DESTINATION_USERNAME,
    WPpassword = process.env.WP_DESTINATION_PASSWORD,
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

exports.WPHeaders = {
    headers : {
        "Authorization" : WPauth,
        "Content-Type" : "application/json",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json"
    },
    WPMediaHeaders : {
        "Authorization": WPauth,
        "Content-Type": "application/x-www-form-urlencoded",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json",
        "Content-Disposition": "attachment;"
    }
};
