const express = require('express');
const app = express();
const path = require('path')
const port = 3006;
const fs =require('fs');

// const pushData = require(path.join(__dirname + '/push.js'))
// const scrapData = require(path.join(__dirname + '/reviews.js'))

const getDataFromWp = require(path.join(__dirname + '/index.js'))
const sendDataToWp = require(path.join(__dirname + '/push.js'))

app.get('/',function(req, res){

    res.sendFile(path.join(__dirname + '/client/index.html'));

})

app.get('/getDataFromWP',function(req, res){
    console.log(req.query.ID)

    function next(){
        res.sendFile(path.join(__dirname + '/client/refresh.html'));
    }

    getDataFromWp.init(req.query.ID , next)

})


app.get('/refresh',function(req, res){


    const shell = require('./child_helper');

    const commandList = [
        "pm2 restart all"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

    res.sendFile(path.join(__dirname + '/client/feedWp.html'));

})



app.get('/sendDataToWP',function(req, res){
    console.log(req.query.DOMAIN)
    console.log(req.query.USERNAME)
    console.log(req.query.PASSWORD)

    function next(){
        res.sendFile(path.join(__dirname + '/client/success.html'));
    }

    sendDataToWp.init(req.query.DOMAIN,req.query.USERNAME,req.query.PASSWORD , next)



})


app.get('/finish', function (req, res) {

    // fs.unlink(path.join(__dirname + '/static/data.json'))
    //
    // fs.unlink(path.join(__dirname + '/static/roomsData.json'))
    fs.stat(path.join(__dirname + '/static/reviews/WPdata.json'), function (err, stats) {
        console.log(stats);//here we got all information of file in stats variable

        if (err) {
            return console.error(err);
        }

        fs.unlink(path.join(__dirname + '/static/reviews/WPdata.json'),function(err){
            if(err) return console.log(err);
            console.log('file deleted successfully');
        });
    });
    

    const shell = require('./child_helper');

    const commandList = [
        "pm2 reload all"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

    res.sendFile(path.join(__dirname + '/client/done.html'));

})




const server = app.listen(port);
console.log(port)
server.timeout = 0;
