require('dotenv').config();

const request = require('request');
const fs =require('fs');

const WPplaceID = parseInt(process.env.WP_SOURCE_TERMID);//term ID

console.log(WPplaceID)
const download = require('./download');

let info,map;

let cc = 0;

const REQUEST = function(url,postType) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
             console.log(response.statusCode)

            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let OUTPUT = []

                    info = JSON.parse(body)

                    info.map(function(item) {

                        //return array with length 0, if can not find the term
                        let split,url = '';

                        if( item.websites[0] === 56){
                            item.websites[0] = 'TRIPADVISOR'
                        }
                        if( item.websites[0] === 74){
                            item.websites[0] = 'BOOKING'
                        }
                        if(item.languages[0] === 67){
                            item.languages[0] = 2
                        }
                        if(item.languages[0] === 66){
                            item.languages[0] = 1
                        }
                        map = {
                            title : item.title.rendered,
                            content : item.content.rendered,
                            name : item.acf.name,
                            date : item.acf.date,
                            score : item.acf.score,
                            link : item.acf.link,
                            staydate : item.acf.staydate,
                            review_from : item.websites[0],
                            language : item.languages[0]
                        }

                        OUTPUT.push(map)

                    })

                    let JsonUrl = './static/' + postType + '/WPdata.json';

                    fs.writeFile( JsonUrl , JSON.stringify(OUTPUT) , 'utf8',(err)=> {
                        console.log("change data output")
                        resolve(cc++)
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    })

                }

            }else{
                console.log(error)
            }


        });
    });

}

exports.init = REQUEST;