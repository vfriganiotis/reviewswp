require('dotenv').config()

let WPusername = process.env.WP_SOURCE_USERNAME,
    WPpassword = process.env.WP_SOURCE_PASSWORD,
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

exports.WPHeaders = {
    headers : {
        "Authorization" : WPauth,
        "Content-Type" : "application/json",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json"
    },
    method: "GET"
};
