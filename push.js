const GetTermsId = require('./WPDestination/ReadTermsIDFromWp')
const Check = require('./WPDestination/checkIfJsonExist')
const PushData = require('./WPDestination/pushData')

// function GetTermsFromWP() {
//     GetTermsId.call(WPpushData,ObjTermsIds)
// }

const SEND_DATA_TO_WP = function(DOMAIN,USERNAME,PASSWORD,next) {

    let Credentials = {
        domain : DOMAIN,
        username : USERNAME,
        password : PASSWORD
    }

    Check.checkJSON(PushData.push, Credentials , next)


}

exports.init = SEND_DATA_TO_WP;



// GetTermsFromWP()

//
// function checkIfExistJson(){
//     try {
//         if (fs.existsSync('./static/museums/WPdata.json')) {
//             let  DataMuseumsRaw = require('./static/museums/WPdata');
//             if( DataMuseumsRaw.data.length > 0){
//                 PushData(DataMuseumsRaw,ObjTermsIds.museum)
//             }
//         }
//     } catch(err) {
//         console.error(err)
//     }
//
//     try {
//         if (fs.existsSync('./static/beaches/WPdata.json')) {
//             let DataBeachesRaw  = require('./static/beaches/WPdata');
//             if(DataBeachesRaw.data.length > 0){
//                 PushData(DataBeachesRaw ,ObjTermsIds.beaches)
//             }
//         }
//     } catch(err) {
//         console.error(err)
//     }
//
//     try {
//         if (fs.existsSync('./static/directions/WPdata.json')) {
//             let DataDirectionsRaw = require('./static/directions/WPdata.json');
//             if( DataDirectionsRaw.data.length > 0 ){
//                 PushData(DataDirectionsRaw,ObjTermsIds.sightseeing)
//             }
//         }
//     } catch(err) {
//         console.error(err)
//     }
// }
//
// function REQUESTWP(url,postType) {
//
//     return new Promise((resolve, reject) => {
//         request( url , function(error, response, body){
//             //Create
//             if (!error && response.statusCode === 201) {
//                 //Gallery Media
//                 if( typeof body !== 'object'){
//                     let info = JSON.parse(body)
//                     postType.data[postType.count].img = info.id
//                 }else{
//                     //CREATE ROOM
//                     console.log("Map Created")
//                 }
//             }
//
//             if (!error && response.statusCode === 200) {
//
//                 if (typeof body === 'string') {
//
//                     let info = JSON.parse(body)
//
//                     info.map(function(item){
//                         console.log(item.slug)
//                         switch(item.slug) {
//                             case 'beaches':
//                                 // code block
//                                 ObjTermsIds[item.slug] = item.id
//                                 break;
//                             case 'museum':
//                                 ObjTermsIds[item.slug] = item.id
//                                 break;
//                             case 'sightseeing':
//                                 ObjTermsIds[item.slug] = item.id
//                                 break;
//                             default:
//                             // code block
//                         }
//                     })
//
//                     console.log(ObjTermsIds)
//                 }
//
//             }
//
//             resolve("Created" + response.statusCode)
//
//         });
//     });
// }


// async function readTermIDFromWP(callback) {
//
//     const WP = {
//         url: WPterms,
//         headers: {
//             "Authorization": WPauth,
//             "Content-Type": "application/json",
//             "Access-Control-Allow-Origin": "*",
//             "Accept": "application/json"
//         },
//         method: "GET"
//     }
//
//     const ReadTerm = await Request.init(WP, '', ObjTermsIds)
//
//     console.log("eeeeee")
//     //Run the Async request
//     callback()
// }

// async function PushData(DATA,termId){
//     let count = 0;
//
//     for ( const item of DATA.data){
//
//         //Get Count i
//         DATA.count = count;
//
//         //Check if Image is not empty
//         if( item.img !== '' ){
//
//             let formData = {
//                 file: [fs.createReadStream(item.img)],
//             };
//
//             let WPMediaPost = {
//                 url : WPImages,
//                 headers : {
//                     "Authorization" : WPauth,
//                     "Content-Type" : "application/x-www-form-urlencoded",
//                     "Access-Control-Allow-Origin": "*",
//                     "Accept": "application/json",
//                     "Content-Disposition" : "attachment;"
//                 },
//                 method: "POST",
//                 formData: formData
//             };
//
//             const w = await Request.init(WPMediaPost,DATA,ObjTermsIds)
//
//             //increment count for DataMuseumsRaw.data[DataPagesRaw.count] after wp create image id
//
//         }
//         count++
//
//     }
//
//     for (const item of DATA.data) {
//
//         //if empty IMG send value -1
//         if( item.img === ""){
//             item.img = -1
//         }
//
//         const JsonBody = {
//             "title" : "Jedi",
//             "status" : "publish"
//         }
//
//         JsonBody.title = item.title;
//         JsonBody.content = item.content;
//         JsonBody.featured_media = item.img;
//         JsonBody.fields = {
//             lat : item.lat,
//             long : item.lng
//         }
//         JsonBody.sightseeings = termId;
//
//
//         const WPCreatePost = {
//             url : WPMaps,
//             headers : {
//                 "Authorization" : WPauth,
//                 "Content-Type" : "application/json",
//                 "Access-Control-Allow-Origin": "*",
//                 "Accept": "application/json"
//             },
//             method: "POST",
//             body: JsonBody,
//             json: true
//         }
//
//         const v = await Request.init(WPCreatePost,DATA,ObjTermsIds)
//
//     }
// }


