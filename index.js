const GET_DATA_FROM_WP = function(ID,next) {

    console.log("OK " + ID)
    const getData = require('./WPSource/init')
    const Authentication = require('./WPSource/authentication')

    const WPReviews = "https://reviews.sitesdemo.com/wp-json/wp/v2/reviews?hotels=" + ID + "&per_page=100",
        WPBeaches = "https://reviews.sitesdemo.com/" + process.env.WP_SOURCE_LANG + "/wp-json/wp/v2/beaches",
        WPSigthSeeings = "https://reviews.sitesdemo.com/" + process.env.WP_SOURCE_LANG + "/wp-json/wp/v2/worthseeings";

    console.log(WPReviews)
    const dataRequest = [
        {
            WP: {
                url: WPReviews,
                Authentication
            },
            slug: 'reviews'
        }
        // {
        //     WP: {
        //         url : WPBeaches,
        //         Authentication
        //     },
        //     slug: 'beaches'
        // },
        // {
        //     WP: {
        //         url : WPSigthSeeings,
        //         Authentication
        //     },
        //     slug: 'worthseeings'
        // }
    ]

    getData.init(dataRequest,next)

}

exports.init = GET_DATA_FROM_WP;